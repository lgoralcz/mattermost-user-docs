# Mattermost @ CERN

## Service information

The Mattermost service provides a chat system to the CERN community. Its main purpose is to provide
a place for professional communication among colleagues, but also with external collaborators.

The URL of the Mattermost service is <https://mattermost.web.cern.ch>; this can be used in your
web browser, or alternatively when adding the server to the official [mobile or desktop apps][mm-apps].

## Documentation

This site documents CERN-specific features and pecularities; for a general guide on how to use
Mattermost please consult the [official user guide][mm-guide] provided by the Mattermost developers.

## Support

If you need support, multiple channels (no pun intended) are available:

- There's the [~mattermost channel][it-mm] in the IT-dep Mattermost team (only accessible to full CERN
  accounts). This is the best place to get in touch with other Mattermost users and the team running
  the CERN Mattermost service. For general questions regarding Mattermost usage and features, this
  is ideal as you will benefit from both community knowledge and also the expertise of the Mattermost
  team.
- You can open a [SNOW ticket][snow]; this is the best option if you cannot access Mattermost for
  some reason or simply prefer to open a ticket.
- The Mattermost service manager, [Adrian Mönnich][adrian-phonebook], is available on Mattermost as
  well, so you can send a direct message to [@amonnich][adrian-dm]. Please keep in mind that for
  generic questions the other channels are usually sufficient and often more efficient.

!!! tip
    You're always welcome to ask in our Mattermost channel regardless of your question/problem.
    If needed we'll ask you to switch to a DM or open a SNOW ticket.


[mm-apps]: https://mattermost.com/download/
[mm-guide]: https://docs.mattermost.com/guides/channels.html
[it-mm]: https://mattermost.web.cern.ch/it-dep/channels/mattermost
[snow]: https://cern.service-now.com/service-portal/?id=sc_cat_item&name=incident&fe=mattermost
[adrian-phonebook]: https://phonebook.cern.ch/search?q=amonnich
[adrian-dm]: https://mattermost.web.cern.ch/_redirect/messages/@amonnich

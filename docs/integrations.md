# Integrations & Plugins

## GitLab

The [GitLab integration plugin][gitlab-plugin] is installed on the CERN Mattermost instance.
Please note that it only works with [CERN GitLab][gitlab-cern], not the public gitlab.com

You can connect your GitLab account to it by clicking the corresponding icon on the sidebar.

To subscribe notifications for a project on GitLab, you need to create a webhook first. This can be
done using the `/gitlab webhook add GROUP[/REPO]` slash command.

Then check the [plugin docs][gitlab-plugin-slash-docs] on how to use the `/gitlab subscriptions`
slash command to subscribe your channel to the events you are interested in.

!!! tip
    If you find the daily notification DMs from the plugin about open reviews, PRs, etc. annoying,
    you can disable them using these slash commands:

        /gitlab settings notifications off
        /gitlab settings reminders off


## GitHub

The [GitHub integration plugin][github-plugin] is installed on the CERN Mattermost instance.

You can connect your GitHub account to it by clicking the corresponding icon on the sidebar.

To subscribe notifications for an organization or repo on GitHub, you need to
[create a webhook][github-plugin-webhook-docs] on GitHub (either on the organization or repo level)
pointing to [Mattermost's GitHub endpoint][mm-github-hook]. Contact [@amonnich][adrian-dm] to get
the webhook secret that needs to be entered when creating the webhook.

Then check the [plugin docs][github-plugin-slash-docs] on how to use the `/github subscriptions`
slash command to subscribe your channel to the events you are interested in.

!!! tip
    If you find the daily notification DMs from the plugin about open reviews, PRs, etc. annoying,
    you can disable them using these slash commands:

        /github settings notifications off  
        /github settings reminders off


## JIRA

You can get JIRA notifications in your Mattermost channel. Please see [KB0005213][kb-jira] for
details on how to set this up. You need to create an incoming webhook in Mattermost and then follow
the instructions in the KB article.

!!! tip "Important"
    When creating the JIRA webhook don't forget to replace the normal Mattermost URL with that of
    the JIRA bridge (`https://mattermost.web.cern.ch/....` --> `https://jira-mm.web.cern.ch/...`).



## Autolink

Certain strings, such as GitHub and GitLab issue links, SNOW ticket numbers and JIRA references
are automatically converted into nicer-looking formats and/or clickable links.

If you would like to have additional links converted in your team or channels, provide us with 
example data (e.g. the link and how it should be displayed, or the text and how it should be
converted to a clikable link) and we can most likely set this up for you.

!!! info
    Restricting this feature by team/channel won't work for direct messages as these are never
    linked to any particular team, so any request that should include direct messages (or not be
    restricted to specific teams/channels) needs to be specific enough to not cause problems as it
    will apply to every mattermost message someone sends.


## Chanrestrict

This custom plugin allows you to make a channel read-only for all users except those explicitly
allowed, so it's useful if you have an announcement channel where you don't want everyone to write.

To enable it, use `/chanrestrict enable` in the channel. From that point on, nobody (not even
yourself) can send a message to the channel.

To grant write privileges in the channel, use `/chanrestrict grant @<username>` and if you want to
revoke them again, use `/chanrestrict revoke @<username>`.

The posting restriction can be lifted again using `/chanrestrict disable`.

This plugin can also make a channel mandatory for all team members; they will be automatically
added when joining the team or if they attempt to leave it. To enable this feature, simply use
`/chanrestrict mandatory on` (and `/chanrestrict mandatory off` to disable).  

Making a channel mandatory does not affect users already in the team, but you can use
`/chanrestrict force-join` to add everyone to the channel who wasn't in there yet. This can only be
used if the channel is marked as mandatory. **Please use this feature with care especially in large
teams in order to not annoy your users.**

!!! note
    Using the commands from this plugin is only possible if you are a Team Admin.


## CERNphone

If you are logged in with a CERN account and view the user info popup of another user, their
CERNphone number will be displayed.

Clicking it will call it using the CERNphone desktop client if you have it installed.

![](assets/cernphone.png)


## Zoom

On top of a channel there's a small camera icon. Clicking it will start a Zoom meeting using your
Personal Meeting ID in the channel (or direct message) that's currently active. You can also use
`/zoom start` instead of clicking the button.

The first time you do this, you will be asked to give Mattermost some permissions on your Zoom
account; afterwards clicking it will immediately post the meeting join information in the channel.


!!! bug "Known issues"
    Sometimes the passcode is missing in the link. Re-authenticating with Zoom by first doing
    `/zoom disconnect` and then starting the meeting again usually helps.


## Other plugins

The following plugins are installed and provided as-is. We recommend checking their documentation
or asking in the [~mattermost][it-mm] channel if you need help with them.

- [Matterpoll][matterpoll-plugin]
- [Remind Bot][remind-bot-plugin]
- [Standup Raven][standup-raven-plugin]



[gitlab-plugin]: https://github.com/mattermost/mattermost-plugin-gitlab
[gitlab-cern]: https://gitlab.cern.ch
[github-plugin-slash-docs]: https://mattermost.gitbook.io/plugin-gitlab/setup/configuration#step-4-subscribe-to-projects-and-groups
[github-plugin]: https://github.com/mattermost/mattermost-plugin-github
[github-plugin-webhook-docs]: https://github.com/mattermost/mattermost-plugin-github#step-2-create-a-webhook-in-github
[mm-github-hook]: https://mattermost.web.cern.ch/plugins/github/webhook
[github-plugin-slash-docs]: https://github.com/mattermost/mattermost-plugin-github#slash-commands
[adrian-dm]: https://mattermost.web.cern.ch/_redirect/messages/@amonnich
[kb-jira]: https://cern.service-now.com/service-portal?id=kb_article&n=KB0005213
[it-mm]: https://mattermost.web.cern.ch/it-dep/channels/mattermost
[matterpoll-plugin]: https://github.com/matterpoll/matterpoll#usage
[remind-bot-plugin]: https://github.com/scottleedavis/mattermost-plugin-remind#usage
[standup-raven-plugin]: https://github.com/standup-raven/standup-raven/blob/master/docs/user_guide.md

# FAQ

## Accounts

### I have a very long full name, can I shorten it?

Your name on Mattermost is synchronized from the central databases, but you can enter a "Nickname"
which will be displayed instead of your full name.

!!! tip
    If you would like to use a different/shorter name, e.g. because you have many first/last names
    but only use one of them actively or because you typically use a shortened version of your official
    first name, you can set your preferred names via the [CERN phonebook][pref-names].
    This will affect Mattermost, Indico, and many other services. It may take a bit to synchronize
    and you need to logout and login again to Mattermost for it to be updated there.


### I returned to CERN on a new contract and can't login to Mattermost

When trying to login to Mattermost you get this error message instead:

> There is already an account associated with that email address using a sign in method other
> than gitlab. Please sign in using gitlab

This misleading error message (GitLab isn't involved there) usually happens when you log in
with a new CERN account name but your old email address; please open a [SNOW ticket][snow] and we
will fix your account.


### I cannot login on the Desktop app

Make sure you have the latest version installed; you can download it from the [Mattermost website][mm-apps].
If you are using a CERN-managed Windows desktop ("CMF" installed), you can also use CMF to install a
recent version of the Mattermost Desktop app.

!!! tip
    The desktop app is basically a wrapper around the web version. If you always have your web browser
    open anyway, you may want to consider simply using Mattermost directly in your web browser (most
    modern browsers have a "Pin Tab" option to make the tab waste less space and always stay open
    even between browser restarts). The web version is always up to date, and is more resource-friendly
    on your system.


### Can Guest/Social/Lightweight accounts use Mattermost?

Yes. Any account that can login on the (new) CERN SSO can access Mattermost. Individual teams may
be restricted though (see [Who can join a team?](#who-can-join-a-team)).

Since all Mattermost accounts need a username, and we want to avoid collisions with CERN accounts,
anyone logging in with a non-CERN account will get a username based on their email address, e.g.
`somebody-gmail.com` if their email is `somebody@gmail.com`.

!!! warning
    The SSO allows various methods to login as a guest (Guest credentials, Google, GitHub, etc.).
    It is **very important** to always use the same login method. Otherwise you will either get an
    error during login or end up with multiple Mattermost accounts.


### I accidentally logged in with my secondary/service/guest account and this is confusing others

Please contact us; we can disable the account so it doesn't show up anymore.

!!! tip
    Use an incognito window or Firefox's "Container Tabs" feature when logging in with secondary
    or service accounts.



## Team Management

### Who can create new teams?

Anyone with a CERN account (primary/secondary/service) can create a new Mattermost team.
Guest accounts (lightweight/social) cannot create teams.


### Who can join a team?

A newly created team has the following restrictions set:

- Allow any user with an account on this server to join this team: No
- Allow only users with a specific email domain to join this team: cern.ch ("Restricted to CERN
  accounts (no lightweight/social/guest accounts)") [**only teams created after August 1st 2021**]

This means that a new team is **invite-only by default** and will strictly prevent anyone without a
CERN account from joining (they cannot be invited and cannot use the invite link).

The team admin can of course loosen these restrictions:

- By changing *Allow any user with an account on this server to join this team* to **Yes**, anyone
  with a Mattermost account can join. Due to the email domain restriction, this will allow anyone
  with a CERN account (primary/secondary/service) to join the team without having to be explicitly
  invited or using the secret invite link. **Users with no CERN account will not be able to join.**
- By removing the email domain restriction, any Mattermost user - **including non-CERN folks** who
  have just a lightweight/social/guest account - will be able to join the team. Please make sure
  that you actually intend to create such a public team before removing the restriction. Unless
  your team is explicitly targeting members of the general public, it is recommended to keep the
  team private (see the previous bullet point) - that way you can invite people or share the invite
  link with selected people (regardless of their account type) so they can join the team, without
  making it accessible to everyone.


### Can I move channels between teams or merge teams?

Teams cannot be merged, but the Mattermost admins can move channels between teams and thus do
pretty much the equivalent to merging teams (or just move some channels around, depending on what
you need).

For obvious reasons you need to be a Team Admin of the source team(s) (or get the explicit approval
from them) to request this operation.

When contacting us, provide the URLs destination team and the source team + channels.
Any user who is not in the target team will be removed from the channel during the move. We can
provide you with a list of affected users if you prefer to add them to the team before the move.

If you want to move a team's `town-square` channel, you need to provide a new name for it since
channel names need to be unique within a team. For the same reason, please verify that there are no
collisions - you can rename channels yourself if necessary (as always what matters is the name in
the URL, not the "Display Name").

After a move it is recommended for anyone to reload the Mattermost page in the browser (or
reload/restart the desktop app) since the channel may still be displayed in both teams otherwise.
This is purely a client-side display issue though.


### Can I synchronize team membership with an egroup

Yes, this is possible. If you'd like to set this up, please contact us with the following
information:

- Team URL
- Name of the egroup (just one, but it can contain other groups)
- Whether you'd like to add users, remove users, or do both

We can provide you with a list of users that would be added and/or removed. Automatically removing
users also only makes sense if a team is set as invite-only, as users could simply rejoin otherwise.

When removing users in this way, it is NOT possible to exempt some people. You can however create
a new group that contains both your main group (e.g. based on organizational unit or experiment
membership) and a few other users (or groups) who you'd like to keep in your team.

!!! info
    It's also possible to sync channel membership in this way.


### How can I delete my team?

This is an operation that can only be done by the Mattermost admins. Please contact us and include
the **URL** of the team (NOT just the name, we need its unique identifier!).



## Channel Management

### I'm trying to add someone to a private channel, but can't find them

This usually means that the user is not a member of the Mattermost team yet. You need to first
add them to the team (click on the team, and then on "Invite People"), then you can add them to the
channel.

!!! info
    You can only add people to a team if they have a Mattermost account, so they need to have logged
    in to Mattermost at least once.


### I'd like to make an existing public channel private

A Channel Admin can use the "Convert to Private Channel" option in the channel dropdown menu to
make it private. Note that only the Mattermost admins can revert this should it be necessary.


### I'd like to make an existing private channel public

This is an operation that can only be done by the Mattermost admins. Please contact us and include
the **URL** of the channel (NOT just the name, we need its unique identifier!).


### A channel I'm in is too noisy with notifications

You can configure when/how notifications should be sent not only for your Mattermost account but
also individually for channels. It it also possible to mute a channel altogether if you are not
interested in it but don't want to completely leave it. See the
[Mattermost documentation][mm-docs-notifications] for details.



## Integrations

### Can I send messages to a channel/user from a script?

Yes, anyone in a Mattermost team can create webhooks. Please see the [Mattermost docs][mm-dev-docs]
for details on how to use incoming webhooks.


### Can I access the Mattermost API for a custom bot or integration?

First of all, in most cases you do not need to use the API - webhooks are very powerful and may be
all you need. But if you have an advanced usecase where you need full API access, we can enable
Personal Access Tokens for your Mattermost account (or a service account). Please contact us in
this case.

!!! warning
    If you use tokens on your own account, you must not share these tokens with anyone else,
    as they grant full access to your Mattermost account. Handle these tokens like passwords!


### Can you install a Mattermost plugin?

In all likelyhood the answer here is "no". Plugins are very powerful and have unrestricted access
to Mattermost; they are not limited to a channel or team.

If you think a Mattermost plugin may be useful nonetheless, feel free to contact us. If the plugin
provides enough value to the CERN Mattermost community and there are no security concerns, we may
consider it.

Please check [Integration & Plugins][integrations] whether a given plugin is already installed.



[pref-names]: https://phonebook.cern.ch/profile
[snow]: https://cern.service-now.com/service-portal/?id=sc_cat_item&name=incident&fe=mattermost
[mm-apps]: https://mattermost.com/download/#desktop
[mm-dev-docs]: https://developers.mattermost.com/integrate/admin-guide/admin-webhooks-incoming/
[mm-docs-notifications]: https://docs.mattermost.com/messaging/channel-preferences.html#channel-notification-preferences
[integrations]: integrations.md

# Group Integration

By default, all teams and channels in Mattermost are either fully public or private. Public teams
can be joined by anybody (including non-CERN people); private teams are joinable *only* using the
invite link or by being added manually.

To make things easier, we have installed a custom plugin that allows to link a team/channel with an
egroup, allowing people in that egroup to join such a channel on their own.

This only works in the Web and Desktop clients as the mobile Mattermost apps do not support plugins.

!!! info
    If you want to automatically synchronize your team or channel members with an egroup, please
    check the relevant [FAQ entry][faq-teamsync] about how to set this up.


## As a user who wants to join a restricted team

In the main menu (icon with three horizontal lines), click "**Join Restricted Teams**".

This will go through an authentication flow which is usually transparent, but if you haven't logged
in to CERN SSO recently you may need to do so again.

Afterwards, a dialog appears showing you all teams you can join based on your current group membership.
Clicking a team will add you to it immediately.

!!! info
    Due to technical limitations in Mattermost, you need to be a member of some team before you can
    use this functionality!


## As a user who wants to join a restricted channel

In the main menu (icon with three horizontal lines), click "**Join Restricted Channels**". Make sure
you are currently in the team containing the channel.

This will go through an authentication flow which is usually transparent, but if you haven't logged
in to CERN SSO recently you may need to do so again.

Afterwards, a dialog appears showing you all channels you can join based on your current group
membership. Clicking a channel will add you to it immediately.


## As a Team Admin who wants to use this in their team

In the team settings make sure your team has "**Allow any user with an account on this server to
join this team**" set to "**No**" (this is the default, but check anyway).

Then, from any channel of the team use the slash command `/cern team-group XXX` with `XXX` being the
name of the egroup.   You will get a message indicating that the group has been set.

To remove the link between the team and a group again, use `/cern team-group *`.

!!! info
    Due to technical limitations in Mattermost, users need to be a member of some team before they
    can join a team using this feature.
    
    If the team is for a department/group/etc. and likely to be the first team a newcomer will join,
    you may want to send the usual team invitation link to them during your onboarding procedures so
    they can join the team without having to use the self-join feature.

!!! warning
    There is no automated synchronization with this feature, i.e. people who have been removed from
    the egroup WILL NOT be automatically removed from the team! However, we can set up an automated
    synchronization for this. See the [FAQ entry][faq-teamsync] for details.



## As a Channel Admin who wants to use this in their channel

Make sure your channel is private (see the [FAQ][faq-convert-private] in case it isn't).

Then, while in the channel you want to link to a group, use the slash command
`/cern channel-group XXX` with `XXX` being the name of the egroup. You will get a message indicating
that the group has been set.

To remove the link between the channel and a group again, use `/cern channel-group *`.

Team admins can use the `/cern list-channels` command to get a list of all channels in the current
team that have groups set.

!!! warning
    There is no automated synchronization at the moment, i.e. people who have been removed from the
    egroup WILL NOT be automatically removed from the channel! However, we can set up an automated
    synchronization for this. See the [FAQ entry][faq-teamsync] for details.

[faq-teamsync]: faq.md#can-i-synchronize-team-membership-with-an-egroup
[faq-convert-private]: faq.md#id-like-to-make-an-existing-public-channel-private
